// Bit&Black 2018

(function($) {
    
    var matomoRoot = null; 
    var matomoTrackingURL = null;
    var htmlSelector;

    $.fn.piwikOptOut = function(property) { 
        $(this).matomoOptOut(property);
        return this;
    };
            
    $.fn.matomoOptOut = function(property) { 
        
        htmlSelector = this;
        matomoRoot = property.matomoRoot; 
        matomoTrackingURL = matomoRoot + '/index.php?module=API&method=AjaxOptOut';
        
        $.fn.matomoOptOut.getMatomoTrackStatus(); 
        
        $('label p', htmlSelector)
            .css({
                'display': 'inline'
            });
        
        $(htmlSelector)
            .on('click', 'label', function() {
                $.fn.matomoOptOut.setMatomoTrackStatus($(this).attr('data-matomotrack'));
            });
    
        return this;
    };

    $.fn.matomoOptOut.getMatomoTrackStatus = function() { 
        $.ajax({
            url: matomoTrackingURL + '.isTracked',
            dataType: 'jsonp',     
            data: {
                format: 'json'
            },
            success: function(response) {
                $('[data-matomotrackstatus]').hide();
                if (response.value === true) {
                    $('[data-matomotrackstatus="true"]').show();
                    $('label input[type="checkbox"]', htmlSelector).prop('checked', true);
                }
                else {
                    $('[data-matomotrackstatus="false"]').show();
                    $('label input[type="checkbox"]', htmlSelector).prop('checked', false);
                }
                $(htmlSelector).show();
            },
        });  
    } 
    
    $.fn.matomoOptOut.setMatomoTrackStatus = function(status) {
        setter = (status == 'enable') ? '.doTrack' : '.doIgnore';
        $.ajax({
            url: matomoTrackingURL + setter,
            dataType: 'jsonp',     
            data: {
                format: 'json'
            },
            success: function(response) {
                $.fn.matomoOptOut.getMatomoTrackStatus();
            },
        });  
    } 

}(jQuery));
