# piwikOptOut

This jquery plugin is based on the Matomo (Piwik) plugin »Ajax Opt Out« from »Lipperts Web«. You need to install and activate it first.

## Init 

The jquery plugin has to be initialised inside the jquery ready function and needs to know the root of your Matomo installation.

```javascript
$(document).ready(function() {
    
    $('#matomooptout').matomoOptOut({'matomoRoot': 'https://yoursite.com/matomo'});

}); 
```

## HTML 

Embed this code wherever you want.

```
<div id="matomooptout" style="display: none">
    <div data-matomotrackstatus="true">
        <p>
            Sie können sich hier entscheiden, ob in Ihrem Browser ein eindeutiger Webanalyse-Cookie abgelegt werden darf, 
            um dem Betreiber der Website die Erfassung und Analyse verschiedener statistischer Daten zu ermöglichen. 
        </p>
        <p>
            Wenn Sie sich dagegen entscheiden möchten, klicken Sie den folgenden Link, um den Matomo-Deaktivierungs-Cookie in Ihrem Browser abzulegen.
        </p>
        <p>
            <label data-matomotrack="disable">
                <input type="checkbox" checked>
                <span>
                    Ihr Besuch dieser Website wird aktuell von der Matomo Webanalyse erfasst. Klicken Sie hier, damit Ihr Besuch nicht mehr erfasst wird.
                </span>
            </label>
        </p>
    </div>
    <div data-matomotrackstatus="false"> 
        <p>
            Deaktivierung durchgeführt! Ihre Besuche auf dieser Website werden von der Webanalyse nicht mehr erfasst. 
        </p>
        <p>
            Bitte beachten Sie, dass auch der Matomo-Deaktivierungs-Cookie dieser Website gelöscht wird, wenn Sie die in Ihrem Browser abgelegten Cookies entfernen. 
            Außerdem müssen Sie, wenn Sie einen anderen Computer oder einen anderen Webbrowser verwenden, die Deaktivierungsprozedur nochmals absolvieren.
        </p>
        <p>
            <label data-matomotrack="enable">
                <input type="checkbox">
                <span>
                    Ihr Besuch dieser Website wird aktuell von der Matomo Webanalyse nicht erfasst. Klicken Sie hier, um Ihren Besuch wieder erfassen zu lassen.
                </span>
            </label>
        </p>
    </div>
</div> 
```

## Help 

Feel free to contact us under hello@bitandblack.com
